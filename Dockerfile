FROM novinrepo:8082/docker/python:3.7-alpine

COPY ./pip.conf /root/.pip/pip.conf

RUN mkdir /code
WORKDIR /code

ENV FLASK_DEBUG=production
COPY ./requirements.txt /code/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY ./app /code/app

CMD [ "python" , "/code/app/app.py" ]