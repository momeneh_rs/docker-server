
from routers import app
import logging

if __name__ == '__main__':
    FORMAT = '[%(asctime)s] [%(levelname)s] [process_pid:%(process)d] %(message)s'
    logging.basicConfig(format=FORMAT, datefmt='%Y-%m-%d %H:%M:%S')

    app.run(host="0.0.0.0", port=8080)
