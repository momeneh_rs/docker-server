from flask import Flask, request

app = Flask(__name__)


@app.route('/hello')
def hello():
    name = request.args.get('name')
    if name is None:
        return 'Hello Stranger'
    else:
        return "hello " + name


@app.route('/author')
def author():
    return 'Momeneh Rasouli'


@app.route('/health')
def health():
    return '', 200
