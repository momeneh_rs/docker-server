# **Server Docker**


[![Python](https://img.shields.io/badge/python-3.7-green)](https://www.python.org/downloads/release/python-370/)
[![Flask](https://img.shields.io/badge/Flask-2.2.2-blue)](https://flask.palletsprojects.com/en/2.1.x/)


<h2> Installation </h2>
<ul>
	<li> Install requirements: 
		<ol type="a">
			<li> Install <b> "python3.7" </b> </li>
			<li>  Install <b> "virtualenv" </b>
        			<pre><code>pip install virtualenv</code></pre> 
			</li>
			<li>
        			Create the virtual environment using following command:
        			<pre><code>virtualenv YourVirtualenvName</code></pre>
    			</li>
			<li> Active virtualenv:
				<ul>
					<li> For linux: 
       						<pre><code>source YourVirtualenvName/bin/activate</code></pre>
					</li>
					<li> For windows:
       						<pre><code>.\YourVirtualenvName\Scripts\activate</code></pre>
					</li>					
				</ul>	
    			</li>
			<li> Now you can install libraries and dependencies listed in requirements file:
        			<ul>
                        <li> For windows:
                                <pre><code>pip install -r requirements.txt</code></pre>
                        </li>					
                    </ul>
            </li>
            <li>
                You can exit from virtual environment using following command:
                <pre><code>deactivate</code></pre>
            </li>
		</ol>
	</li>
			
</ul>

<h2> Running the app </h2>
	<ul>
		<li>Set environment variable:
			<ul>
                <li> For development: 
                        <pre><code>FLASK_ENV=development</code></pre>
                </li>
                <li> For production:
                       <pre><code>FLASK_ENV=production</code></pre>
                </li>					
            </ul>
		</li>
		<li>Run the app:
		    <ul>
                <pre><code>python app.py</code></pre>
            </ul>
		</li>
	</ul>

<h2> Docker </h2>
	<ul>
		<li>
			To build docker image for app:
			<pre><code>docker build . -t server-image-e1</code></pre>
		</li>
        <li>
			Run app:
            <ul>
                <ul>
                    <li>Run on container with docker compose:
                        <pre><code>docker-compose up -d</code></pre>
		            </li>
                    <li>
                        Run on container without docker compose:
                        <pre><code>docker run -d -p 8080:8080 --name container-e1 server-image-e1</code></pre>
                    </li>
                </ul>
            </ul>
        </li>
        <li>
            Execute container:
            <pre><code>docker exec -it container-e1 bash</code></pre>
        </li>
	</ul>